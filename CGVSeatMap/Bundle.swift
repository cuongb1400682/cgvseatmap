//
//  Utilities.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/6/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation

extension Bundle {
    func loadJSONData(fromResource name: String) -> [String: Any] {
        guard let sampleMapURL = Bundle.main.url(forResource: name, withExtension: "json") else {
            return [:]
        }
        
        guard let urlData = try? Data(contentsOf: sampleMapURL, options: .mappedIfSafe) else {
            return [:]
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: urlData, options: .mutableLeaves) as? [String: Any] else {
            return [:]
        }
        
        return json
    }
}
