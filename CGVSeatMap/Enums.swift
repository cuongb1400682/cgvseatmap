//
//  Enums.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

enum SeatType: String {
    case occupied = "OCCUPIED"
    case selected = "SELECTED"
    case fourDX = "4DX"
    case standard = "STANDARD"
    case vip = "VIP"
    case lAmour = "LAMOUR"
    case couple = "COUPLE"
    case deluxe = "DELUXE"
    case premium = "PREMIUM"
    case handicapped = "HANDICAPPED"
    case sweetbox = "SWEET_BOX"
    case goldClass = "GOLD_CLASS"

    var color: UIColor {
        switch self {
        case .occupied: return  #colorLiteral(red: 0.8551752567, green: 0.8382009864, blue: 0.795809865, alpha: 1)
        case .selected: return  #colorLiteral(red: 0.6778645515, green: 0.1684414744, blue: 0.1939454675, alpha: 1)
        case .fourDX: return  #colorLiteral(red: 0.501627028, green: 0.494674325, blue: 0.0894645825, alpha: 1)
        case .standard: return  #colorLiteral(red: 0.6052119136, green: 0.5446673036, blue: 0.489818871, alpha: 1)
        case .vip: return  #colorLiteral(red: 0.4951862097, green: 0.1917163134, blue: 0.2543068826, alpha: 1)
        case .lAmour: return  #colorLiteral(red: 0.2913685739, green: 0.2352535427, blue: 0.1918350756, alpha: 1)
        case .couple: return  #colorLiteral(red: 0.3886480331, green: 0.1731246412, blue: 0.5023894906, alpha: 1)
        case .deluxe: return  #colorLiteral(red: 0.1798205376, green: 0.2475250959, blue: 0.5300191641, alpha: 1)
        case .premium: return  #colorLiteral(red: 0.5705090165, green: 0.6595337391, blue: 0.8161430955, alpha: 1)
        case .handicapped: return  #colorLiteral(red: 0.2086583078, green: 0.507917583, blue: 0.2661124468, alpha: 1)
        case .sweetbox: return  #colorLiteral(red: 0.8446699977, green: 0.1405992508, blue: 0.4107247591, alpha: 1)
        case .goldClass: return  #colorLiteral(red: 0.6119807363, green: 0.3882972002, blue: 0.05104986578, alpha: 1)
        }
    }
}

enum Colors {
    case textColor
    
    var rawValue: UIColor {
        switch self {
        case .textColor: return #colorLiteral(red: 1, green: 0.9125101737, blue: 0.9525556376, alpha: 1)
        }
    }
}
