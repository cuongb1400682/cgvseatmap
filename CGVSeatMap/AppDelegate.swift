//
//  AppDelegate.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let seatMapViewController = SeatMapViewController()
        
        let navigationController = UINavigationController()
        navigationController.viewControllers = [seatMapViewController]
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = navigationController
        return true
    }

}

