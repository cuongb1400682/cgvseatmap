//
//  MapDescription.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

class MapDescription: UIStackView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init() {
        super.init(frame: .zero)
        axis = .horizontal
        alignment = .fill
        distribution = .fillEqually

        [[.occupied, .selected, .fourDX],
         [.standard, .vip, .lAmour],
         [.couple, .deluxe, .premium],
         [.handicapped, .sweetbox, .goldClass]].map(createColumn(_:)).forEach(addArrangedSubview(_:))
    }
    
    func createColumn(_ column: [SeatType]) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 6
        column.map(createItem(_:)).forEach(stackView.addArrangedSubview(_:))
        return stackView
    }
    
    func createItem(_ seatType: SeatType) -> UIStackView {
        let colorBox = SeatButton(seatType: seatType)
        NSLayoutConstraint.activate(
            [colorBox.widthAnchor.constraint(equalToConstant: 18),
             colorBox.heightAnchor.constraint(equalToConstant: 18)]
        )

        let typeLabel = UILabel()
        typeLabel.text = seatType.rawValue
            .split(separator: "_")
            .map { $0.capitalized }
            .joined(separator: " ")
        typeLabel.textColor = Colors.textColor.rawValue
        typeLabel.font = .systemFont(ofSize: 12)

        let stackView = UIStackView()
        stackView.spacing = 4
        stackView.addArrangedSubview(colorBox)
        stackView.addArrangedSubview(typeLabel)
        
        return stackView
    }
    
}
