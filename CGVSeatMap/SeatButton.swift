//
//  SeatButton.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

class SeatButton: UIButton {
    public var seatType: SeatType {
        didSet {
            updateViews()
        }
    }
    
    public var isOccupied: Bool {
        return seatType == .occupied
    }
    
    public var isSelectedSeat: Bool {
        return seatType == .selected
    }
        
    required init?(coder: NSCoder) {
        seatType = .standard

        super.init(coder: coder)
    }
    
    init(seatType: SeatType) {
        self.seatType = seatType
        
        super.init(frame: .zero)

        titleLabel?.font = .systemFont(ofSize: 14)
        setTitleColor(Colors.textColor.rawValue, for: .normal)
        updateViews()
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        if isOccupied {
            super.setTitle(nil, for: state)
        } else {
            super.setTitle(title, for: state)
        }
    }
    
    private func updateViews() {
        backgroundColor = seatType.color
        isEnabled = !isOccupied
    }
}

extension SeatButton {
    
    override func draw(_ rect: CGRect) {
        if isOccupied {
            drawCross()
        } else if isSelectedSeat {
            drawTriangle()
        }
    }
    
    private func drawTriangle() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let size = bounds.width

        context.saveGState()
        context.setFillColor(Colors.textColor.rawValue.cgColor)
        context.setLineWidth(1)
        
        context.move(to: CGPoint(x: size - 8, y: 0))
        context.addLine(to: CGPoint(x: size, y: 0))
        context.addLine(to: CGPoint(x: size, y: 8))
        context.addLine(to: CGPoint(x: size - 8, y: 0))
        context.fillPath()

        context.restoreGState()
    }
    
    private func drawCross() {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let size = bounds.width
        
        context.saveGState()
        context.setStrokeColor(Colors.textColor.rawValue.cgColor)
        context.setLineWidth(1)
        
        context.move(to: CGPoint(x: 0, y: 0))
        context.addLine(to: CGPoint(x: size, y: size))
        
        context.move(to: CGPoint(x: size, y: 0))
        context.addLine(to: CGPoint(x: 0, y: size))
        
        context.strokePath()
        context.restoreGState()
    }

}
