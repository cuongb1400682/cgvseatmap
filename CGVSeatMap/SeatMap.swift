//
//  SeatMap.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit
import CartesianProduct

protocol SeatMapDelegate: class {
    func seatMap(_ seatMap: SeatMap, selected seat: UIButton, at indexPath: IndexPath)
}

class SeatMap: UIStackView {
    public var delegate: SeatMapDelegate?
    
    public var rowCount: Int {
        return seatMatrix?.rowCount ?? 0
    }
    
    public var colCount: Int {
        return seatMatrix?.colCount ?? 0
    }
    
    public var seatMatrix: SeatMatrix? {
        didSet {
            updateViews()
        }
    }
    
    private var seats: [[SeatButton]] = []
    
    init() {
        self.seats = []

        super.init(frame: .zero)

        self.axis = .vertical
        self.spacing = 1
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public subscript(_ rowIndex: Int, _ colIndex: Int) -> SeatButton {
        return seats[rowIndex][colIndex]
    }
    
    private func updateViews() {
        self.seats = []
        arrangedSubviews.forEach(removeArrangedSubview(_:))
        
        for rowIndex in 0 ..< rowCount {
            self.seats.append(
                (0 ..< colCount).map { colIndex in
                    createSeat(rowIndex, colIndex)
                }
            )
        }
        
        seats.map(createSeatRow).forEach(addArrangedSubview(_:))
    }
    
    private func createSeatRow(_ seatRow: [UIButton]) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 1
        seatRow.forEach(stackView.addArrangedSubview(_:))
        
        return stackView
    }
    
    private func createSeat(_ rowIndex: Int, _ colIndex: Int) -> SeatButton {
        let currentSeatType = seatMatrix?[rowIndex, colIndex].type ?? .standard
        let seat = SeatButton(seatType: currentSeatType)
        let rowCharacter = Character(UnicodeScalar(UInt8(rowIndex + 65)))
        
        seat.setTitle("\(rowCharacter)\(colIndex + 1)", for: .normal)
        seat.addTarget(self, action: #selector(seatPressed(_:)), for: .touchUpInside)
        NSLayoutConstraint.activate(
            [seat.widthAnchor.constraint(equalToConstant: 34),
             seat.heightAnchor.constraint(equalToConstant: 34)]
        )

        return seat
    }
    
    public func setSelect(_ rowIndex: Int, _ colIndex: Int, _ isSelected: Bool) {
        if isSelected {
            seats[rowIndex][colIndex].seatType = .selected
        } else {
            let currentSeatType = seatMatrix?[rowIndex, colIndex].type ?? .standard
            seats[rowIndex][colIndex].seatType = currentSeatType
        }
    }
    
    @objc func seatPressed(_ seat: UIButton) {
        let isPressedButton = { (indices: (Int, Int)) -> Bool in
            let (rowIndex, colIndex) = indices
            return self.seats[rowIndex][colIndex] === seat
        }

        guard
            let (rowIndex, colIndex) = product(0 ..< rowCount, 0 ..< colCount).first(where: isPressedButton)
            else { return }
        
        let pressedButton = seats[rowIndex][colIndex]
        delegate?.seatMap(self, selected: pressedButton, at: IndexPath(row: rowIndex, section: colIndex))
    }
    
}
