//
//  Box.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation

class Box<T> {
    typealias Listener = (_ newValue: T) -> Void
    
    public var listener: Listener? {
        didSet {
            listener?(value)
        }
    }
    
    public var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
}

extension Box: Equatable where T: Equatable {
    
    static func == (lhs: Box<T>, rhs: Box<T>) -> Bool {
        return lhs.value == rhs.value
    }
    
}

extension Box: Hashable where T: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
    
}
