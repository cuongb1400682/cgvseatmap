//
//  SeatMatrix.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation

class SeatMatrix {
    public typealias Element = (type: SeatType, isSelected: Bool)
    public typealias Listener = (Int, Int, Element) -> Void
    
    private var matrix: [[Box<Element>]] = []
    
    public var rowCount: Int {
        return matrix.count
    }
    
    public var colCount: Int {
        return matrix.isEmpty ? 0 : matrix[0].count
    }
    
    public var listener: Listener?
    
    init(_ rowCount: Int, _ colCount: Int) {
        for rowIndex in 0 ..< rowCount {
            matrix.append(
                (0 ..< colCount).map { colIndex in
                    let element = Box<Element>((.standard, false))
                    element.listener = { value in
                        self.triggerElementChanged(rowIndex, colIndex, value)
                    }
                    return element
                }
            )
        }
    }
    
    public subscript(_ rowIndex: Int, _ colIndex: Int) -> Element {
        get {
            return matrix[rowIndex][colIndex].value
        }
        
        set(newValue) {
            matrix[rowIndex][colIndex].value = newValue
        }
    }
    
    private func triggerElementChanged(_ rowIndex: Int, _ colIndex: Int, _ newValue: Element) {
        listener?(rowIndex, colIndex, newValue)
    }
}
