//
//  ViewController.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit
import SimpleLayout_Swift

class SeatMapViewController: UIViewController {
    
    private lazy var mapContainer: UIStackView = {
        let container = UIStackView()
        container.axis = .vertical
        container.alignment = .center
        container.distribution = .fill
        container.addArrangedSubview(screenLabel)
        container.addArrangedSubview(seatMap)
        container.addArrangedSubview(mapDescription)
        container.setCustomSpacing(44, after: seatMap)
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    private lazy var screenLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 40, weight: .bold)
        label.text = "SCREEN"
        label.textColor = UIColor(white: 0.2, alpha: 1.0)
        label.heightAnchor.constraint(equalToConstant: 100).isActive = true
        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .black
        scrollView.maximumZoomScale = 3.0
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 48, bottom: 44, right: 48)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var seatMap: SeatMap = {
        let seatMap = SeatMap()
        seatMap.translatesAutoresizingMaskIntoConstraints = false
        seatMap.delegate = self
        return seatMap
    }()
    
    private lazy var mapDescription: MapDescription = {
        let mapDescription = MapDescription()
        return mapDescription
    }()
    
    private lazy var seatMatrixViewModal: SeatMatrixViewModal = {
        let viewModal = SeatMatrixViewModal()
        viewModal.seatMatrix.listener = self.resetSeatMapWithNewMatrix
        return viewModal
    }()
    
    private lazy var summary: Summary = {
        let summary = Summary()
        summary.translatesAutoresizingMaskIntoConstraints = false
        summary.delegate = self
        return summary
    }()
    
    private lazy var minimap: Minimap = {
        let minimap = Minimap(originView: scrollView)
        minimap.translatesAutoresizingMaskIntoConstraints = false
        return minimap
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateViews()
        loadSeatMapFromNetwork()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateMinZoomScaleForSize(view.bounds.size)
        minimap.updateSnapshot()
    }
    
    func updateViews() {
        title = "CGV Seat Map"
        
        view.backgroundColor = .white
        view.addSubview(scrollView)
        view.addSubview(summary)
        view.addSubview(minimap)
        
        scrollView.layout
            .top()
            .leading()
            .trailing()
        scrollView.addSubview(mapContainer)
        
        mapContainer.layout.fill()
        
        summary.layout
            .leading(by: view)
            .trailing(by: view)
            .top(by: scrollView, attribute: .bottom)
            .bottom(by: view.safeAreaLayoutGuide)
        
        minimap.layout
            .top(by: view.safeAreaLayoutGuide, 2)
            .leading(by: view, 4)
    }
    
    func loadSeatMapFromNetwork() {
        seatMatrixViewModal.fetchSeatMap(onComplete: { seatMatrix in
            seatMatrix.listener = self.selectSeat
            self.seatMatrixViewModal.seatMatrix.value = seatMatrix
        }, onError: { error in
            print(error)
        })
    }

    func selectSeat(_ rowIndex: Int, _ colIndex: Int, _ newValue: SeatMatrix.Element) {
        seatMap.setSelect(rowIndex, colIndex, newValue.isSelected)
    }
    
    func resetSeatMapWithNewMatrix(_ newMatrix: SeatMatrix?) {
        if let newMatrix = newMatrix {
            seatMap.seatMatrix = newMatrix
            view.layoutIfNeeded()
            scrollToCenterX()
        }
    }
    
    func scrollToCenterX() {
        let centerX = (scrollView.contentSize.width - scrollView.frame.width) / 2
        scrollView.setContentOffset(CGPoint(x: centerX, y: 0), animated: true)
    }
}

extension SeatMapViewController: SeatMapDelegate {
    func showLimitedSeatMessage() {
        UIAlertController.showMessage(title: "You can only select maximum 8 seats",
                                      message: nil)
    }
    
    func changeSelectedSeat(_ willSelected: Bool, _ seatLabel: String) {
        if willSelected {
            seatMatrixViewModal.selectedSeat.insert(seatLabel)
        } else {
            seatMatrixViewModal.selectedSeat.remove(seatLabel)
        }
        
        summary.price = seatMatrixViewModal.totalPrice
    }
    
    func seatMap(_ seatMap: SeatMap, selected seat: UIButton, at indexPath: IndexPath) {
        let rowIndex = indexPath.row
        let colIndex = indexPath.section
        let seatLabel = seat.titleLabel?.text ?? ""
        let willBeSelected = !seatMatrixViewModal.selectedSeat.contains(seatLabel)
        
        if willBeSelected && seatMatrixViewModal.selectedSeat.count >= 8 {
            showLimitedSeatMessage()
            return
        }
        
        seatMatrixViewModal.toggleSelected(rowIndex, colIndex)
        changeSelectedSeat(willBeSelected, seatLabel)
    }
}

extension SeatMapViewController: UIScrollViewDelegate {
    private func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / mapContainer.bounds.width
        let heightScale = size.height / mapContainer.bounds.height
        let minScale = min(widthScale, heightScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mapContainer
    }
}

extension SeatMapViewController: SummaryDelegate {
    func summary(_ summary: Summary, onBookNowButtonPress: UIButton) {
        seatMatrixViewModal.bookSeat()
    }
}
