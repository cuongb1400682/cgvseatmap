//
//  SeatMapViewModals.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation
import CartesianProduct

class SeatMatrixViewModal {
    public var seatMatrix: Box<SeatMatrix?> = Box<SeatMatrix?>(nil)
    
    public var selectedSeat: Set<String> = []
    
    public var pricePerSeat: Int = 0
    
    public var totalPrice: Int {
        return pricePerSeat * selectedSeat.count
    }
    
    public func toggleSelected(_ rowIndex: Int, _ colIndex: Int) {
        if let (type, isSelected) = seatMatrix.value?[rowIndex, colIndex] {
            seatMatrix.value?[rowIndex, colIndex] = (type: type, isSelected: !isSelected)
        }
    }

    public func fetchSeatMap(onComplete: ((SeatMatrix) -> Void)?,
                             onError: ((String) -> Void)? = nil) {
        let json = Bundle.main.loadJSONData(fromResource: "sampleMap")
        
        guard let (width, height, stringMatrix, price) = self.retrieveMatrixData(fromJSON: json) else {
            onError?("Cannot load json data from sampleMap.json")
            return
        }
        
        let seatMatrix = self.createSeatMatrix(from: stringMatrix, height, width)
        pricePerSeat = price
        onComplete?(seatMatrix)
    }
    
    private func retrieveMatrixData(fromJSON json: [String: Any]) ->
        (Int, Int, [[String]], Int)? {
        guard
            let width = json["width"] as? Int,
            let height = json["height"] as? Int,
            let matrix = json["matrix"] as? [[String]],
            let price = json["price_per_seat"] as? Int
        else {
            return nil
        }
        
        return (width, height, matrix, price)
    }
    
    private func createSeatMatrix(from stringMatrix: [[String]], _ height: Int, _ width: Int) -> SeatMatrix {
        let seatMatrix = SeatMatrix(height, width)
        
        product(0 ..< height, 0 ..< width).forEach { indices in
            let (rowIndex, colIndex) = indices
            let type = SeatType(rawValue: stringMatrix[rowIndex][colIndex]) ?? .standard
            
            seatMatrix[rowIndex, colIndex].type = type
        }
        
        return seatMatrix
    }

    public func bookSeat() {
        if selectedSeat.isEmpty {
            UIAlertController.showMessage(
                title: "Sorry",
                message: "You must select at least 1 seat."
            )
        } else {
            // Networking code for booking seats
            
            UIAlertController.showMessage(
                title: "Thank you",
                message: "Seats at \(selectedSeat.joined(separator: ", ")) have been booked successfully."
            )
        }
    }
    
}
