//
//  Summary.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/7/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

protocol SummaryDelegate {
    func summary(_ summary: Summary, onBookNowButtonPress: UIButton)
}

class Summary: UIView {
    
    var delegate: SummaryDelegate?
    
    private lazy var currencyFormatter: NumberFormatter = {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.maximumFractionDigits = 0
        return currencyFormatter
    }()
    
    private lazy var movieName: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "STAR WAR: THE EMPIRE STRIKES BACK"
        label.font = .systemFont(ofSize: 13, weight: .bold)
        return label
    }()
    
    private lazy var movieDescription: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0.2, alpha: 1.0)
        label.text = "2D English Sub"
        label.font = .systemFont(ofSize: 13)
        return label
    }()
    
    private lazy var priceTotal: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = currencyFormatter.string(from: 0)
        label.font = .systemFont(ofSize: 13, weight: .semibold)
        return label
    }()
    
    public var price: Int = 0 {
        didSet {
            priceTotal.text = currencyFormatter.string(from: NSNumber(value: price))
        }
    }
    
    private lazy var bookButton: UIButton = {
        let buttonHeight: CGFloat = 24
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: .bold)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Book Now", for: .normal)
        button.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.isUserInteractionEnabled = true
        return button
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init() {
        super.init(frame: .zero)
        updateViews()
    }
    
    func updateViews() {
        backgroundColor = .white
        
        addSubview(movieName)
        movieName.layout
            .leading(12)
            .top(8)
        
        addSubview(movieDescription)
        movieDescription.layout
            .leading(by: movieName)
            .top(by: movieName, attribute: .bottom, 4)
        
        addSubview(priceTotal)
        priceTotal.layout
            .leading(by: movieDescription)
            .top(by: movieDescription, attribute: .bottom, 12)
            .bottom(-8)
        
        addSubview(bookButton)
        bookButton.layout
            .trailing(-16)
            .centerY()
        bookButton.addTarget(self, action: #selector(bookButtonPressed), for: .touchDown)
    }
    
    @objc func bookButtonPressed() {
        delegate?.summary(self, onBookNowButtonPress: bookButton)
    }
}
