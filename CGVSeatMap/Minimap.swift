//
//  Minimap.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/7/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

class Minimap: UIImageView {
    public var originView: UIScrollView? {
        didSet {
            updateSnapshot()
        }
    }
    
    private lazy var heightConstraint = heightAnchor.constraint(equalToConstant: 60)
    
    private lazy var widthConstraint = widthAnchor.constraint(equalToConstant: 72)
    
    private lazy var focusRing: UIView = {
        let view = UIView()
        NSLayoutConstraint.activate(
            [view.widthAnchor.constraint(equalToConstant: 25),
             view.heightAnchor.constraint(equalToConstant: 25)]
        )
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.red.cgColor
        view.backgroundColor = UIColor(white: 0, alpha: 0.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init(originView: UIScrollView) {
        self.originView = originView
        
        super.init(frame: .zero)
        
        NSLayoutConstraint.activate([widthConstraint, heightConstraint])
        backgroundColor = .black
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.04835249352, green: 0.04835249352, blue: 0.04835249352, alpha: 1)
        layer.opacity = 0.5
        
        addSubview(focusRing)
        focusRing.layout
            .top()
            .leading()
    }
    
    public func updateSnapshot() {
        takeSnapshotImage()
        resizeToFit()
    }
    
    public func moveFocusRing() {
        guard
            let originBounds = originView?.bounds,
            let contentSize = originView?.contentSize
            else { return }
        
        
    }
    
    private func resizeToFit() {
        guard let originBounds = originView?.bounds else {
            return
        }
        
        if originBounds.height > 0 {
            let ratio = CGFloat(originBounds.width) / CGFloat(originBounds.height)
            
            widthConstraint.constant = heightConstraint.constant * ratio
            layoutIfNeeded()
        }
    }
    
    private func takeSnapshotImage() {
        guard let originView = originView else {
            return
        }
        
        UIGraphicsBeginImageContextWithOptions(originView.bounds.size, false, 1.0)
        if let context = UIGraphicsGetCurrentContext() {
            originView.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        } else {
            image = nil
        }
        UIGraphicsEndImageContext()
    }
}
