//
//  UIAlertController.swift
//  CGVSeatMap
//
//  Created by Cuong Nguyen on 11/7/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    public static func showMessage(title: String? = nil, message: String? = nil) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let rootVC = appDelegate?.window?.rootViewController
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        rootVC?.present(alert, animated: true, completion: nil)
    }
    
}
